﻿using AODL.Document.Content.Tables;
using AODL.Document.Content.Text;
using AODL.Document.SpreadsheetDocuments;
using ClosedXML.Excel;

namespace ExcelTestCreateRead
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateExcel();
            CreateODS();
        }



        private static void CreateODS()
        {
            //Create new spreadsheet document
            SpreadsheetDocument spreadsheetDocument = new SpreadsheetDocument();
            spreadsheetDocument.New();
            //Create a new table
            Table table = new Table(spreadsheetDocument, "First", "tablefirst");
            //Create a new cell, without any extra styles 

            int theRow = 1;
            for (int x = 1; x < 13; x++)
            {

                for (int y = 1; y < 13; y++)
                {
                    
                    
                    table.InsertCellAt(theRow, 1, CreateSimpleCell(spreadsheetDocument, table, x.ToString()));
                    table.InsertCellAt(theRow, 2, CreateSimpleCell(spreadsheetDocument, table, "*"));
                    table.InsertCellAt(theRow, 3, CreateSimpleCell(spreadsheetDocument, table, y.ToString()));
                    table.InsertCellAt(theRow, 4, CreateSimpleCell(spreadsheetDocument, table, string.Format("=A{0}*C{0}",theRow)));
                    theRow++;
                }
            }
            //Insert table into the spreadsheet document
            spreadsheetDocument.TableCollection.Add(table);
            spreadsheetDocument.SaveTo(@"C:\temp\multipication.ods");      
        }

        private static Cell CreateSimpleCell(SpreadsheetDocument spreadsheetDocument, Table table, string text)
        {
            Cell cell = table.CreateCell();
            cell.OfficeValueType = "string";
            Paragraph paragraph = ParagraphBuilder.CreateSpreadsheetParagraph(spreadsheetDocument);
            
            paragraph.TextContent.Add(new SimpleText(spreadsheetDocument, "Some text"));
            cell.Content.Add(paragraph);
            return cell;
        }
        private static void CreateExcel()
        {
            var multiplication = new XLWorkbook();
            var sheet = multiplication.Worksheets.Add("Multipication");

            int theRow = 1;
            for (int x = 1; x < 13; x++)
            {

                for (int y = 1; y < 13; y++)
                {

                    sheet.Cell(theRow, 1).SetValue(x);
                    sheet.Cell(theRow, 2).SetValue("*");
                    sheet.Cell(theRow, 3).SetValue(y);
                    sheet.Cell(theRow, 4).SetFormulaR1C1("RC[-3]*RC[-1]"); //R=row C=column and is assumed current so that we can do +&- on R and C
                    theRow++;
                }
            }
            multiplication.SaveAs(@"C:\temp\multipication.xlsx");
            
        }

       
    }
}
